# 💾 🇪🇺
# Stordata i EU

Note:

- Er åpen for spørsmål gjennom hele presentasjonen.
- Ikke noe her er hogget i stein. La oss skape denne sesjonen sammen. Jeg har litt opplegg, men sendes gjerne en retning dere synes er mer interessant.
- Skru gjerne av Mute.



---

<!-- .slide: data-background="https://bestanimations.com/Animals/Mammals/Cats/cats/cute-kitty-animated-gif-4.gif" data-background-size="10%" data-background-position="top" -->


<div class="halfsplit">
<div class="gridleft" style="background-color:#ffffff00;"> 


<svg class="r-stretch" width="3.92cm" height="3.92cm" viewBox="0 0 37 37" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
	<g id="QRcode">
		<rect x="0" y="0" width="37" height="37" fill="#ffffff"/>
		<path style="stroke:#000000" transform="translate(4,4.5)" d="M0,0h1M1,0h1M2,0h1M3,0h1M4,0h1M5,0h1M6,0h1M9,0h1M12,0h1M15,0h1M17,0h1M18,0h1M19,0h1M20,0h1M22,0h1M23,0h1M24,0h1M25,0h1M26,0h1M27,0h1M28,0h1M0,1h1M6,1h1M8,1h1M11,1h1M12,1h1M13,1h1M15,1h1M18,1h1M22,1h1M28,1h1M0,2h1M2,2h1M3,2h1M4,2h1M6,2h1M8,2h1M9,2h1M10,2h1M11,2h1M12,2h1M15,2h1M17,2h1M18,2h1M19,2h1M22,2h1M24,2h1M25,2h1M26,2h1M28,2h1M0,3h1M2,3h1M3,3h1M4,3h1M6,3h1M9,3h1M10,3h1M11,3h1M14,3h1M18,3h1M19,3h1M22,3h1M24,3h1M25,3h1M26,3h1M28,3h1M0,4h1M2,4h1M3,4h1M4,4h1M6,4h1M8,4h1M13,4h1M15,4h1M16,4h1M17,4h1M18,4h1M22,4h1M24,4h1M25,4h1M26,4h1M28,4h1M0,5h1M6,5h1M8,5h1M9,5h1M11,5h1M13,5h1M15,5h1M19,5h1M22,5h1M28,5h1M0,6h1M1,6h1M2,6h1M3,6h1M4,6h1M5,6h1M6,6h1M8,6h1M10,6h1M12,6h1M14,6h1M16,6h1M18,6h1M20,6h1M22,6h1M23,6h1M24,6h1M25,6h1M26,6h1M27,6h1M28,6h1M8,7h1M9,7h1M12,7h1M14,7h1M17,7h1M18,7h1M0,8h1M1,8h1M3,8h1M6,8h1M7,8h1M10,8h1M13,8h1M14,8h1M16,8h1M22,8h1M23,8h1M24,8h1M26,8h1M27,8h1M0,9h1M2,9h1M3,9h1M4,9h1M5,9h1M8,9h1M10,9h1M11,9h1M16,9h1M20,9h1M21,9h1M22,9h1M25,9h1M28,9h1M0,10h1M1,10h1M2,10h1M3,10h1M4,10h1M6,10h1M8,10h1M9,10h1M10,10h1M13,10h1M16,10h1M17,10h1M19,10h1M20,10h1M21,10h1M23,10h1M25,10h1M26,10h1M27,10h1M0,11h1M1,11h1M3,11h1M4,11h1M5,11h1M7,11h1M8,11h1M13,11h1M15,11h1M16,11h1M18,11h1M19,11h1M26,11h1M27,11h1M1,12h1M3,12h1M6,12h1M8,12h1M12,12h1M13,12h1M14,12h1M15,12h1M16,12h1M17,12h1M18,12h1M19,12h1M20,12h1M21,12h1M22,12h1M23,12h1M25,12h1M27,12h1M28,12h1M1,13h1M2,13h1M3,13h1M5,13h1M10,13h1M11,13h1M12,13h1M13,13h1M16,13h1M19,13h1M20,13h1M21,13h1M2,14h1M6,14h1M12,14h1M13,14h1M14,14h1M21,14h1M22,14h1M25,14h1M26,14h1M27,14h1M28,14h1M1,15h1M2,15h1M4,15h1M5,15h1M7,15h1M8,15h1M9,15h1M12,15h1M13,15h1M14,15h1M15,15h1M16,15h1M17,15h1M18,15h1M19,15h1M20,15h1M22,15h1M23,15h1M25,15h1M27,15h1M0,16h1M1,16h1M4,16h1M6,16h1M7,16h1M9,16h1M11,16h1M13,16h1M14,16h1M16,16h1M17,16h1M19,16h1M21,16h1M23,16h1M27,16h1M1,17h1M3,17h1M4,17h1M9,17h1M14,17h1M16,17h1M22,17h1M23,17h1M25,17h1M28,17h1M0,18h1M2,18h1M5,18h1M6,18h1M8,18h1M10,18h1M13,18h1M17,18h1M22,18h1M23,18h1M24,18h1M27,18h1M28,18h1M3,19h1M4,19h1M10,19h1M11,19h1M13,19h1M17,19h1M18,19h1M23,19h1M27,19h1M28,19h1M0,20h1M3,20h1M6,20h1M7,20h1M8,20h1M9,20h1M10,20h1M11,20h1M12,20h1M14,20h1M15,20h1M16,20h1M19,20h1M20,20h1M21,20h1M22,20h1M23,20h1M24,20h1M26,20h1M8,21h1M11,21h1M12,21h1M13,21h1M16,21h1M17,21h1M19,21h1M20,21h1M24,21h1M26,21h1M27,21h1M28,21h1M0,22h1M1,22h1M2,22h1M3,22h1M4,22h1M5,22h1M6,22h1M8,22h1M9,22h1M10,22h1M11,22h1M12,22h1M13,22h1M14,22h1M20,22h1M22,22h1M24,22h1M27,22h1M0,23h1M6,23h1M10,23h1M11,23h1M12,23h1M13,23h1M16,23h1M18,23h1M20,23h1M24,23h1M25,23h1M26,23h1M27,23h1M0,24h1M2,24h1M3,24h1M4,24h1M6,24h1M9,24h1M11,24h1M14,24h1M16,24h1M17,24h1M20,24h1M21,24h1M22,24h1M23,24h1M24,24h1M0,25h1M2,25h1M3,25h1M4,25h1M6,25h1M8,25h1M9,25h1M10,25h1M11,25h1M13,25h1M14,25h1M19,25h1M20,25h1M21,25h1M22,25h1M23,25h1M24,25h1M25,25h1M26,25h1M27,25h1M0,26h1M2,26h1M3,26h1M4,26h1M6,26h1M11,26h1M13,26h1M14,26h1M19,26h1M24,26h1M25,26h1M26,26h1M28,26h1M0,27h1M6,27h1M8,27h1M9,27h1M11,27h1M15,27h1M16,27h1M17,27h1M18,27h1M19,27h1M20,27h1M22,27h1M23,27h1M27,27h1M0,28h1M1,28h1M2,28h1M3,28h1M4,28h1M5,28h1M6,28h1M8,28h1M9,28h1M10,28h1M12,28h1M13,28h1M15,28h1M17,28h1M18,28h1M20,28h1M22,28h1M23,28h1M25,28h1M27,28h1"/>
	</g>
</svg>


https://lassegs.gitlab.io/geo-fou-forum/

</div>
<div class="gridright">

**Lasse Gullvåg Sætre**

[Enabling technologies](https://www.forskningsradet.no/portefoljer/muliggjorende-teknologier/)

lgs@rcn.no

[Forskningsrådet](forskningsradet.no)

NCP Horizon CL3 & DIGITAL, NCC-NO, ++

</div>
</div>

Note:

- Bakt inn interaktivitet, slå opp om dere vil. Ligger ganske mye info inne i presentasjonen, så tror den kan være nyttig å ha.
- Si litt om bakgrunn og min motivasjon for dette.
- IT-nerd som ville studere samfunnsfag, fant GIS.
- Startet MaptimeOSL - web mapping.
- Jobbet med jordobservasjon i FN.
- Vi har mye å lære av europeiske geomiljø.
- Norsk geokompetanse svært høy, verden har mye å tjene på at vi eksporter den.

---

<!-- .slide: data-background="https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/_1200x800_fit_center-center_82_none/Forskningsradet_Temabilde_Arrangementer_TeknologiDigitalisering_01_@1x_RGB.jpg" data-background-opacity="0.33"-->

## Agenda

0. Intro
1. Hackpad
2. Bakgrunn og policy
3. Utlysninger i 24
4. Tilbud fra Forskningsrådet

Note:

Her er vi forbi intro. Innmaten er i 

---

<iframe data-src="https://cryptpad.fr/pad/#/2/pad/edit/lqF7wFutqGzU4eFk1KFCHMh8/embed/" class="r-stretch"></iframe>

---

<!-- .slide: data-background="https://kumospace.mo.cloudinary.net/https://content.kumospace.com/hubfs/Find-the-Best-Online-Workspace.jpg?tx=w_responsive:fallback-max-width_1255;fallback-max-width-mobile_720" data-background-size="35%" data-background-position="top right" -->



- [Europeisk stordatastrategi](https://digital-strategy.ec.europa.eu/en/policies/strategy-data)

- [Data Governance Act](https://digital-strategy.ec.europa.eu/en/policies/data-governance-act)

- [Digital Decade](https://digital-strategy.ec.europa.eu/en/policies/europes-digital-decade): 2 milliarder euro til datadeling


Note:

- Europeisk stordatastrategi: EU har utviklet en stordatastrategi for å sikre sin globale konkurranseevne og data suverenitet. Strategien fokuserer på å skape ett enkelt, indre marked for data, slik at flere data blir tilgjengelige for bruk i økonomi og samfunn, samtidig som selskaper og individer som genererer dataen, har kontrollen.
- Data Governance Act (DGA) er en ny europeisk lovgivning som har som mål å regulere og styrke håndteringen av data i EU.
  - DGA vil ha betydelige konsekvenser for norske aktører som KATVERKET, SINTEF, UIB, NINA og SSB, da de vil måtte tilpasse seg de nye kravene og retningslinjene som loven introduserer.
  - Loven vil legge vekt på beskyttelse av personopplysninger og sikring av datakvalitet, noe som vil kreve at aktørene implementerer robuste og sikre systemer for datahåndtering.
  - DGA vil også introdusere nye regler for deling og utveksling av data, både innenlands og på tvers av landegrensene. Dette vil påvirke hvordan norske aktører samarbeider og deler data med andre europeiske organisasjoner.
  - For å oppfylle kravene i DGA vil norske aktører måtte etablere tydelige ansvarsstrukturer og implementere effektive mekanismer for datastyring og overvåkning. Dette vil også kreve at aktørene investerer i kompetanseutvikling og opplæring for å sikre at de har nødvendig kunnskap og ferdigheter til å etterleve lovens krav. 
  - På lang sikt kan DGA bidra til å styrke tilliten til norske aktører og deres evne til å håndtere data på en sikker og pålitelig måte, både nasjonalt og internasjonalt.
- I løpet av tiåret planlegger EU å investere €2 milliarder i europeiske prosjekt for å utvikle databehandlingsinfrastrukturer, delingsverktøy, arkitekturer og styringsmekanismer for å fremme datadeling. 

---

# Data Altruism <small>(noun)</small>

*the practice where individuals or companies voluntarily and freely share the data they generate for the common good. This concept is part of the EU's new rules for data governance, aimed at boosting data sharing in Europe.*

<small> 

[kilde](https://digital-strategy.ec.europa.eu/en/policies/strategy-data) 

</small>



Note:
- DGA skisserer konseptet med dataaltruisme - å gjøre data frivillig tilgjengelig av individer eller selskaper for fellesgoder (som medisinske forskningsprosjekter). For å øke tilliten til konseptet med dataaltruisme, og for å oppmuntre individer og selskaper til å donere data til slike organisasjoner som skal brukes til det bredere samfunnsgode, etablerer DGA muligheten for organisasjoner som engasjerer seg i dataaltruisme, å registrere seg som en 'dataaltruismeorganisasjon anerkjent i EU'.

---


<!-- .slide: data-background-color="#d6e82b" -->

<span class="r-fit-text">🗺️ vs ⛰️</span>

---

<!-- .slide: data-background-image="https://www.ruralyoutheurope.com/wp-content/uploads/2021/05/Untitled-design-2-2048x1024.png" data-background-size="contain" -->



Note: 

Horizon Europe er EUs niende rammeprogram for forskning og innovasjon.

Mål: Fremme forskning og innovasjon for å takle samfunnsutfordringer og styrke den europeiske konkurransedyktigheten.

Økonomiske rammer: Ca. €95,5 milliarder for perioden 2021-2027.
Varighet: 7-års program (2021-2027).

Nedslagsfelt: Inkluderer helse, miljø, digitalisering, samfunnssikkerhet og mat, m.m..

TRL-nivå: Fra grunnleggende forskning til implementering av løsninger.


---

<!-- .slide: data-background-image="https://pbs.twimg.com/media/FCH_mhjXEAE12xP?format=jpg&name=large" data-background-size="contain" -->

Note:

Mål: Fremme digital transformasjon i Europa ved å styrke teknologisk suverenitet og øke konkurransekraften.

Økonomiske rammer: Omtrent €9,2 milliarder for perioden 2021-2027.

Varighet: 7-års program (2021-2027).

Nedslagsfelt: Fokuserer på kunstig intelligens, cybersikkerhet, superdatamaskiner, digitale ferdigheter, og bred digitalisering.

TRL-nivå: Fra teknologisk utvikling til demonstrasjon og implementering.

---

<!-- .slide: data-auto-animate -->

<ul>
    <li>Horisont Europa</li>
    <li>Digital Europa</li>
</ul>

---

<!-- .slide: data-auto-animate -->

<ul>
<li>Horisont Europa</li>
    <ul>
        <li>Klynge 4 Digital</li>
        <li>Klynge 4 Space</li>
        <li>EuroHPC</li>
        <li>EuroHPC 2024</li>
    </ul>
<li>Digital Europa</li>
    <ul>
        <li>SO2 Data</li>
        <li>SO2 Data 2024</li>
    </ul>      
</ul>

Note:

- Dette er ikke en uttømmende liste over stordata-relaterte utlysninger i Horisont eller Digital. Langt der i fra. Jeg rekker ikke alle.
- Dette kan være litt kjedelig, men siden dette ikke er powerpoint men klikkbare lenker håper jeg dere kan leve med meg. 

---

## Utlysningstyper

<small>

- **Innovasjonsaksjoner (IA)**
  - Fokus på teknologi klar for markedet
  - Finansieringsgrad: Opptil 70% (for profitt), 100% (for non-profit)

- **Forsknings- og Innovasjonsaksjoner (RIA)**
  - Fremmer grunnleggende/fremragende forskning
  - Finansieringsgrad: 100%

- **Samordnings- og Støtteaksjoner (CSA)**
  - Støtter nettverksbygging og samordning
  - Finansieringsgrad: 100%

- **Enkeltilskudd (Simple grants)**
  - Forenklet tilgang til finansiering for enkeltprosjekter
  - Finansieringsgrad: Varierer, ofte 50%  

</small>

---

<!-- .slide: data-background="https://horizoneurope.gr/wp-content/uploads/2020/11/hub-horizon.png" data-background-size="10%" data-background-position="top right" -->

### 🖥️ HEU CL4 Digital 

15.11.23 - 19.3.24

<small>

| Topic | Call for Proposal | Type of action |
|-------|--------------------|-----------------|
| [AI-driven data operations and compliance technologies](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl4-2024-data-01-01) | CL4-2024-DATA-01-01 | IA |
| [Piloting emerging Smart IoT Platforms and decentralized intelligence](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl4-2024-data-01-03) | CL4-2024-DATA-01-03 | IA |
| [Platform Building, standardisation and Up-scaling of the ‘Cloud-Edge-IoT’ Solutions](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl4-2024-data-01-05) | CL4-2024-DATA-01-05 | CSA |
| [Open Source for Cloud/Edge to support European Digital Autonomy](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl4-2024-digital-emerging-01-21) | CL4-2024-DIGITAL-EMERGING-01-21 | RIA |
| [Industrial leadership in AI, Data and Robotics boosting competitiveness and the green transition](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl4-2024-digital-emerging-01-04) | CL4-2024-DIGITAL-EMERGING-01-04 | IA |

</small>

--

#### AI-drevne dataoperasjoner og overholdelsesteknologier
- Fokuserer på å fremme internasjonalt samarbeid og semantiske webteknologier.
- Kunnskapsområder inkluderer kunstig intelligens og åpne lenkede data.
- Målet er å styrke dataforvaltning og samarbeid gjennom co-programmerte europeiske partnerskap.

--

#### Piloting emerging Smart IoT Platforms and decentralized intelligence
- Utforsker innovasjon innen automatisering og kontrollsystemer og informatikk.
- Tar sikte på å forbedre energieffektivitet gjennom smartere IoT-plattformer.
- Støtter utviklingen av desentralisert intelligens, edge computing og interoperabilitetsstandarder.
--

#### Plattformbygging, standardisering og oppskalering av ‘Cloud-Edge-IoT’ løsninger
- Sikter mot å utvikle cyber-fysiske systemer og edge computing løsninger.
- Fokus på sosiale vitenskaper og humaniora for å forutse teknologiske trender.
- Arbeider for å integrere digitale agenda og støtte innovasjonspolitikk.

--

#### Open Source for Cloud/Edge to support European Digital Autonomy
- Fokus på åpen kildekode for sky- og kantenetverksteknologi.
- Målet er å styrke EUs digitale selvstendighet.

--

#### Industrial leadership in AI, Data and Robotics boosting competitiveness and the green transition
- Tar sikte på industrielt lederskap innen AI, data og robotikk.
- Skal øke konkurranseevnen og fremme grønn overgang.


---

<!-- .slide: data-background="https://horizoneurope.gr/wp-content/uploads/2020/11/hub-horizon.png" data-background-size="10%" data-background-position="top right" -->


### 🚀 HEU CL4 Space 

21.11.23 - 21.3.24

<small>

| Topic                                                | Call for Proposal                | Type of Action    |
| ---------------------------------------------------- | -------------------------------- | ----------------- |
| [Copernicus for Land and Water](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl4-2024-space-01-35) | HORIZON-CL4-2024-SPACE-01-35 | RIA |
| [Quantum Space Gravimetry Phase-B study & Technology Maturation](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl4-2024-space-01-64) | HORIZON-CL4-2024-SPACE-01-64 | RIA |
| [Copernicus for Security](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl4-2024-space-01-36) | HORIZON-CL4-2024-SPACE-01-36 | RIA |
| [Space technologies for European non-dependence and competitiveness](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl4-2024-space-01-73) | HORIZON-CL4-2024-SPACE-01-73 | RIA |

</small>

--

#### Copernicus for Land and Water
- Utvikling av nye metoder for integrasjon av landprodukter i modeller for landoverflate og arealbruk.
- Forbedring av systemer for produkttilbud gjennom innovative metoder og observasjoner for bedre hydrologisk overvåkning.
- Prosjekter bør gi konkrete resultater for Copernicus-tjenesten og støtte automatisering og skalering av prosessene.

--

#### Quantum Space Gravimetry Phase-B study & Technology Maturation
- Støtte EU rompolitikk og EU Green Deal ved å definere en kvantegravimetri pathfinder-misjon.
- Sikre EU suverenitet og ikke-avhengighet for utvikling av kvantegravimetri-kapasiteter.
- Forbedre TRL av kritiske komponenter for å bygge rombasert kvantegravimetri.

--

#### Copernicus for Security
- Forbedret tilpasning av tjenester for å møte utviklende politiske og brukerkrav.
- Utvidelse av tjenestens rekkevidde med nye elementer og brukergrupper.
- Betydelig teknologisk forbedring i deteksjonskapasitet, tilgang til data og levering av informasjon.

--

#### Space technologies for European non-dependence and competitiveness
- Redusere avhengigheten av kritiske teknologier fra utenfor EU for EU-romprogramkomponenter.
- Utvikle eller gjenopprette den europeiske kapasiteten til uavhengig operasjon i rommet.
- Forbedre den tekniske kapasiteten og den overordnede konkurranseevnen til europeiske romindustrileverandører på verdensmarkedet.


---

<!-- .slide: data-background="https://www.etp4hpc.eu/img/image/fotos/eurohpc_logo.jpg?&w=1200&h=706&sx=115.2&sy=0&sw=587.52&sh=345.6&zc=&q=100" data-background-size="25%" data-background-position="top right" -->

## 🖳 EuroHPC

Deadline ~vinter 24

<small>

| Topic | Call for Proposal | Type of Action |
|-------|--------------------|----------------|
| [Innovation Action in Low Latency and High Bandwidth Interconnects](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-eurohpc-ju-2023-inter-02-01) | EUROHPC-JU-2023-INTER-02-01 | IA |
| [Call on Centres Of Excellence For Exascale HPC Applications](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-eurohpc-ju-2023-coe-03-01) | EUROHPC-JU-2023-COE-03-01 | RIA |
| [EuroHPC Virtual Training Academy](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/digital-eurohpc-ju-2023-academy-02-01) | DIGITAL-EUROHPC-JU-2023-ACADEMY-02-01 | DEP CSA |
| [Energy Efficient Technologies in HPC](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-eurohpc-ju-2023-energy-04-01) | EUROHPC-JU-2023-ENERGY-04-01 | RIA |
| [European Quantum Excellence Centres (QECs) in applications for science and industry](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-eurohpc-ju-2023-qec-05-01) | EUROHPC-JU-2023-QEC-05-01 | RIA |

</small>

--

#### Innovation Action in Low Latency and High Bandwidth Interconnects
- Fokuserer på å redusere forsinkelser og øke båndbredden i sammenkoblinger.
- Målet er å forbedre ytelsen og effektiviteten i høytytende databehandling (HPC).
- Støtter innovasjon innen nettverksdesign for neste generasjons HPC-applikasjoner.
--

#### Call on Centres Of Excellence For Exascale HPC Applications
- Utvikler sentre for utmerkelse som spesialiserer seg på exascale HPC-applikasjoner.
- Fokuserer på å fremme kompetanse og kunnskapsdeling i HPC-samfunnet.
- Styrker Europas lederskap og innovasjon i HPC-teknologier og -applikasjoner.
--

#### EuroHPC Virtual Training Academy
- Etablerer en virtuell treningsakademi for å øke ferdigheter innen HPC.
- Tilbyr opplæring og utdanningsressurser til HPC-brukere og fagfolk.
- Fremmer utvikling av et sterkt og kompetent HPC-arbeidskraft i Europa.
--

#### Energy Efficient Technologies in HPC
- Fokuserer på energieffektive teknologier for å redusere HPCs karbonavtrykk.
- Målet er å integrere bærekraftige praksiser i utviklingen av HPC-systemer.
- Støtter forskning på nye metoder for å øke energieffektiviteten i HPC.
--

#### European Quantum Excellence Centres (QECs) in applications for science and industry
- Etablerer sentre for utmerkelse innen kvanteutmerkelse for vitenskap og industri.
- Sikter mot å akselerere utviklingen og anvendelsen av kvanteteknologier.
- Fremmer samarbeid mellom forskningsinstitusjoner og industrien for å drive innovasjon.

---

<!-- .slide: data-background="https://www.etp4hpc.eu/img/image/fotos/eurohpc_logo.jpg?&w=1200&h=706&sx=115.2&sy=0&sw=587.52&sh=345.6&zc=&q=100" data-background-size="25%" data-background-position="top right" -->


## 🖳 EuroHPC kommende 

2024. Kan forandre seg

<small>

| Topic                                                       | Programme          | Indicative Budget (EU funding) |
| ----------------------------------------------------------- | -------------------------- | ----------------- |
| HPC for AI Software Ecosystem                               | Horizon Europe Programme   | €50M (50%)        |
| HPC-QC Applications                                         | Horizon Europe Programme   | €20M (50%)        |
| Support Centre For HPC-powered Artificial Intelligence (AI) Applications | Digital Europe Programme | €5M (50%)         |
| HPC/Cybersecurity/AI                                       | Digital Europe Programme   | €5M (50%)         |
| Continuous Integration and Deployment Platform (CI/CD)      | Digital Europe Programme   | €3M (50%)         |

</small>

--

#### HPC for AI Software Ecosystem
- Utvikling av HPC-arbeidsflyter for å støtte parallellisering av AI-applikasjoner.
- Skalering av AI og AI-data dynamisk for optimal bruk av HPC-kapasiteter.
- Integrering i EuroHPCs fødererte tjenester og supplering til AI-orientert støttesenter.
--

#### HPC-QC Applications
- Støtte utvikling av applikasjoner for europeisk HPC-infrastruktur inkludert HPC-QC systemer.
- Fokus på spesialiserte programvarebiblioteker for modulært design og standardisering.
- Distribusjon til EuroHPC superdatamaskiner og utelukkelse av dobbeltfinansiering.
--

#### Support Centre For HPC-powered Artificial Intelligence (AI) Applications
- Etablering av et støttesenter for å fremme AI-baserte applikasjoner som bruker HPC-evner.
- Tjenester for AI-samfunnet for å vurdere ytelsen til AI-baserte koder i HPC-miljøer.
- Utvikling av tekniske og menneskelige kapasiteter i EuroHPC hostingenheter for AI-applikasjoner.
--

#### HPC/Cybersecurity/AI
- Undersøkelse av cybersikkerhetskrav for sikker tilgang og bruk av HPC-systemer.
- Bidra til EU's Cybersikkerhetsstrategi ved å øke sikkerheten for kritiske infrastrukturer.
- Forbedring av metoder for cybersikkerhetstesting, sertifisering og standarder.
--

#### Continuous Integration and Deployment Platform (CI/CD)
- Utvikle en HPC-programvarestack i form av en felles plattform.
- Bygge på piloten for en EuroHPC CI/CD-plattform for HPC-applikasjoner.
- Sikre at brukerne har tilgang til de nyeste og eksperimentelle versjonene av programvare.

---

<!-- .slide: data-background="https://perin.pt/wp-content/uploads/2020/10/DIGITAL_header-480x207.png" data-background-size="25%" data-background-position="top right" -->

## Digital SO2 Data

28.9.23 - 23.1.24

<small>

| Topic | Call for Proposal | Type of Action |
|-------|--------------------|----------------|
| [Data Space for Cultural Heritage](https://europa.eu/!xDTggq) | DIGITAL-2023-CLOUD-DATA-AI-05-CULTHERITAGE | Simple Grants |
| [Data Space for Tourism](https://europa.eu/!FRNgW4) | DIGITAL-2023-CLOUD-DATA-AI-05-DATATOURISM | Simple Grants |

</small>

--

#### Data Space for Cultural Heritage
- Opprettelse av rammer og verktøy for avansert 3D og XR-data i et kulturarv datarom.
- Samarbeid med europeiske initiativer for 3D-digitalisering og XR-applikasjoner i kulturarv.
- Berikelse av 3D og XR-data tilbudet, og fremme gjenbruk i utdanning og turisme.

--

#### Data Space for Tourism
- Utvikle et sikkert europeisk datarom for turisme for å forbedre produktivitet og bærekraft.
- Forbinde lokale og nasjonale dataøkosystemer og integrere med andre sektorielle datarom.
- Pilotbrukstilfeller for turismedata og samarbeid med Data Spaces Support Centre for felles standarder og arkitektur.


---

<!-- .slide: data-background="https://perin.pt/wp-content/uploads/2020/10/DIGITAL_header-480x207.png" data-background-size="25%" data-background-position="top right" -->

## Digital SO2 Data

2024. Kan forandre seg

<small>

| Topic | Call for Proposal | Type of Action |
|-------|--------------------|----------------|
| Continuation of the action on Cultural Heritage | DIGITAL-2024 | Procurement |
| Continuation of the action on Language Data Space | DIGITAL-2024 | Simple grant |
| European Green Deal Data Space | DIGITAL-2024 | Simple grant |
| Data Space for Skills | DIGITAL-2024 | Simple grant |
| Energy Data Space | DIGITAL-2024 | Simple grant |
| Data Space for Manufacturing (deployment) | DIGITAL-2024 | Simple grant |
| Agricultural Data Space | DIGITAL-2024 | Simple grant |
| Supporting patients' access to their health data | DIGITAL-2024 | Simple grant |
| Data space for public administration | DIGITAL-2024 | Procurement |

</small>

--
#### Continuation of the action on Cultural Heritage
Målet er å utvide datarommet for kulturarv. Dette vil inkludere forbedret desentralisert aggregasjon, utvikling av lisensiering, innhold (inkludert 3D) og metadata-rammeverk, økt flerspråklighet, integrasjon av krav og rammeverk fra felles dataromstjenester, og opprettelse eller konsolidering av koblinger med andre datarom på europeisk, nasjonalt eller lokalt nivå.
--

#### Continuation of the action on Language Data Space
Målet er å utvikle og implementere nye brukstilfeller for Språkdatarommet, med fokus på europeiske industrier og spesielt SMB-er. Ved å skalere opp implementeringen og bruken av Språkdatarommet i innsamling, opprettelse, deling og gjenbruk av språkdata og modeller innen industriene, er målet å etablere et europeisk industrielt språkdataøkosystem.
--

#### European Green Deal Data Space
Målet er å distribuere infrastrukturen for Green Deal-datarommet basert på styringsblåkopien, datoomfanget og implementeringsveikartet som ble definert i prosjektet finansiert under det forrige arbeidsprogrammet og vil bygge på og bli ytterligere muliggjort ved å støtte en rekke initiativer.
--

#### Data Space for Skills
Målet er å utvikle, sette opp og implementere det sikre og pålitelige datarommet, etter anbefalingene fra den forberedende handlingen valgt i den forrige samtalen. Datarommet vil støtte deling og tilgang til ferdighetsdata for ulike formål, fra analytiske og statistiske formål til politikkutvikling eller gjenbruk i innovative applikasjoner.
--

#### Energy Data Space
For å nå EUs ambisiøse mål i energisektoren: øke bruken av fornybare energikilder, redusere nettoenergiimporten og forbedre energieffektiviteten, trenger energisektoren økt tilgjengelighet og tverrsektoriell deling av data, på en kundesentrisk, sikker og pålitelig måte.
--

#### Data Space for Manufacturing (deployment)
Målet med denne handlingen er å skalere opp implementeringen og bruken av datarommene i de to brukskasusene som ble støttet under det forrige arbeidsprogrammet, samtidig som man utvider bruken og opprettholder utviklingen av et europeisk industrielt dataøkosystem.
--

#### Agricultural Data Space
Målet med denne handlingen er å utvikle og distribuere et operativt datarom for deling av landbruksdata. Forventede resultater av denne handlingen er å utvikle et sikkert og pålitelig datarom for å muliggjøre at landbrukssektoren kan dele og få tilgang til data transparent, noe som tillater en økning i dens økonomiske og miljømessige ytelse.
--

#### Supporting patients' access to their health data
Målet er å fremme pasienters tilgang til sine helsedata til primær bruk, og dermed bidra til det foreslåtte europeiske helsedataområdet. Det vil bygge på og skalere opp handlinger under EU4Health-programmet og Digital Europe-programmet for å støtte utrullingen av løsninger som forbedrer pasienters tilgang til dataene sine.
--

#### Data space for public administration
Målet er å skalere opp Offentlige Anskaffelser Datarommet som ble prototypet under det forrige arbeidsprogrammet, forbedre dets analytiske kapasiteter og integrere Smart-mellomvaren for den europeiske Cloud Federation. Det forventes at flere medlemsstater vil koble sine anskaffelsesdata til dette datarommet.


---

Alt (nesten) er hentet fra

- [EU Funding and Tenders portal](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/home)
- Arbeidsprogrammene for [Horisont](https://research-and-innovation.ec.europa.eu/funding/funding-opportunities/funding-programmes-and-open-calls/horizon-europe/horizon-europe-work-programmes_en) og [Digital](https://digital-strategy.ec.europa.eu/en/activities/work-programmes-digital)

---

<!-- .slide: data-background-color="#5c5bee" -->

## 💬

- "Next level", "kalibrerende", "utviklende"

- "Gøy", "interessant", "utfordrende"

- "Grusomt", "veldig vanskelig", "selvtillitsdreper", "aner ikke hva jeg driver med"



---


## 🤓

[Forskningsrådet arrangerer](https://www.forskningsradet.no/arrangementer/) kurs for deltakere, koordinatorer og administratorer på alle nivåer.

- Åpne kaféer
- [HEU Financial Audits](https://www.forskningsradet.no/arrangementer/2023/horizon-europe2020-master-of-finance-and-ec-audits-09-nov23/) 9. november
- [HEU Looking for partners and joining consortia (short webinar)](https://www.forskningsradet.no/arrangementer/2023/a2222.3---looking-for-partners-and-joining-consortia-short-webinar-14-nov/) 14. november
- [HEU - Prosjektutvikling 1](https://www.forskningsradet.no/arrangementer/2023/horisont-europa---prosjektutvikling-1/) 22. november
- [C.1 - Research and Innovation Project Management: the first steps](https://www.forskningsradet.no/arrangementer/2023/research-and-innovation-project-management-the-first-steps-31-mai2/) 29. november

---

<!-- .slide: data-background-color="black" data-background-video="https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/NFR_INTROPOSTER_LONG_NAMETAG_2022-05-05-074407_pwrq.mp4" data-background-size="contain"-->

